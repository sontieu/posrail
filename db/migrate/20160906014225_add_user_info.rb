class AddUserInfo < ActiveRecord::Migration
  def change
    add_column :users, :fullname, :string, default: ""
    add_column :users, :phone, :string, default:  ""
    add_column :users, :facebook, :string, default: ""
  end
end

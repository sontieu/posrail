class AddAuthentication < ActiveRecord::Migration
  def change
    add_column :users, :authenticate_token, :string, default: ""
    add_index :users, :authenticate_token, unique: true
  end
end

class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :created_at, :updated_at, :authenticate_token,
             :phone, :fullname, :facebook
  has_many :products
end

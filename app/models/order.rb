class Order < ActiveRecord::Base
  before_validation :set_total!
  belongs_to :user

  validates :total, presence: true,
            numericality: { greater_than_or_equal_to: 0 }
  validates :user_id, presence: true

  has_many :placements
  has_many :products, through: :placements

  def set_total!
    self.total = products.map(&:price).sum
  end

  def build_placements_with_product_ids_and_quantities(products_info)
    products_info.each do |e|
      arr = e.split(',')
      id, quantity = arr
      self.placements.build(product_id: id, quantity: quantity)
    end
  end
end

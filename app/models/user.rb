class User < ActiveRecord::Base
  validates :authenticate_token, uniqueness: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  before_create :generate_authentication_token!

  has_many :products, dependent: :destroy
  has_many :orders, dependent: :destroy

  def generate_authentication_token!
    begin
      self.authenticate_token = Devise.friendly_token
      printf self.authenticate_token
    end while self.class.exists?(authenticate_token: authenticate_token)
  end


end
